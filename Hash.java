import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hash {


    public static void main(String[] args) {
        System.out.println("Let's code");

        List<Member> members = Member.initMembers();

        Scanner scanner = new Scanner(System.in);




        Member currentMember = null;
        String directive="";
        String input= "start";

        outerLoop:while (!(input.isEmpty())) {
             input = scanner.nextLine();
            if (input.isEmpty()) {break;
            }

            if (input.equals("#i")){
                members = Member.initMembers(input);
            }

            if (input.startsWith("#i")) {
                members = Member.initMembers(input);
            }

            if (input.startsWith("#")&& input.charAt(1) != 'i') {
                directive = input;




                switch (directive) {
                    case "#i":
                        members = Member.initMembers(input);
                        break;
                    case "#1":
                        currentMember = members.get(0);
                        break;
                    case "#2":
                        currentMember = members.get(1);
                        break;
                    case "#3":
                        currentMember = members.get(2);
                        break;
                    case "#4":
                        currentMember = members.get(3);
                        break;
                    case "#5":
                        currentMember = members.get(4);
                        break;
                    case "#p":
                        if (currentMember != null) {
                            System.out.println(currentMember.basicInfo());
                           p:while (true) {
                                String message = scanner.nextLine();
                                if (message.isEmpty()) {
                                    break outerLoop;
                                }
                                if(message.startsWith("#")){
                                    directive = message;
                                    continue ;
                                }
                                if (!message.startsWith("#")) {
                                    // Поиск сообщения в хэш-таблице текущего члена
                                    int index = (int) currentMember.hashFunction(message, currentMember.tableSize);
                                    boolean found = false;
                                    int repetitions = 0;
                                    for (Member.Message msg : currentMember.hashTable[index]) {
                                        if (msg.text.equals(message)) {
                                            found = true;
                                            repetitions = msg.repetitions;
                                            break;
                                        }
                                    }
                                    if (found) {
                                        System.out.println(message + " " + index + " " + repetitions);
                                    }
                                }
                            }
                        } else {
                            System.err.println("Error: No member selected!");
                        }
                        break;
                    case "#a":
                        String message;
                        innerLoop:while (true) {
                            message = scanner.nextLine();
                            if (message.isEmpty()) {
                                break outerLoop;
                            }


                            if (message.startsWith("#")) {
                                directive = message;
                                switch (directive) {
                                    case "#1":
                                        currentMember = members.get(0);
                                        break;
                                    case "#2":
                                        currentMember = members.get(1);
                                        break;
                                    case "#3":
                                        currentMember = members.get(2);
                                        break;
                                    case "#4":
                                        currentMember = members.get(3);
                                        break;
                                    case "#5":
                                        currentMember = members.get(4);
                                        break;
                                    case "#p":
                                        if (currentMember != null) {
                                            System.out.println(currentMember.basicInfo());
                                        }
                                        else  System.err.println("Error: Invalid input! in a");
                                        break;
                                    default:
                                        System.err.println("Error: Invalid input! in a");
                                        break;


                                }
                                break;
                                // Добавлен break после switch внутри #a
                            } else {
                                for (Member member : members) {
                                    member.addMessage(message);
                                }

                            }
                            if (!input.startsWith("#")) {
                                if (currentMember != null) {
                                    currentMember.addMessage(input);
                                }
                            }

                        }
                        break;
                    default:
                        System.err.println("Error: Invalid input!(End)");
                        break;

                }




            }


            }



        }



    static class Member {


        String name;
        int primeNumber = 11;
        int tableSize = primeNumber;

        List<Message>[] hashTable;

        Member(String name, int primeNumber) {
            this.name = name;
            this.primeNumber = primeNumber;
            this.hashTable = new ArrayList[primeNumber]; // Инициализируем массив hashTable
            for (int i = 0; i < primeNumber; i++) {
                hashTable[i] = new ArrayList<>();
            }
        }

        Member(String name, int primeNumber, int tableSize) {
            this.name = name;
            this.primeNumber = primeNumber;
            this.tableSize = tableSize;
            this.hashTable = new ArrayList[tableSize];
            for (int i = 0; i < tableSize; i++) {
                hashTable[i] = new ArrayList<>();
            }
        }

        class Message {
            String text;
            int repetitions;
            int index;

            public Message(String text, int repetitions, int index) {
                this.text = text;
                this.repetitions = repetitions;
                this.index = index;
            }
        }

        public void addMessage(String message) {
            long hash = hashFunction(message, primeNumber);
            int index = (int) (hash % tableSize);

            // Проверяем, содержится ли сообщение уже в хэш-таблице
            boolean messageFound = false;
            for (Message msg : hashTable[index]) {
                if (msg.text.equals(message)) {
                    // Увеличиваем количество повторений сообщения
                    msg.repetitions++;
                    messageFound = true;
                    break;
                }
            }

            // Если сообщение не найдено в хэш-таблице, добавляем его
            if (!messageFound) {
                Message newMessage = new Message(message, 1, index);
                hashTable[index].add(newMessage);
            }

            double loadFactor = (double) countUniqueMessages() / tableSize;
            if (loadFactor >= 0.7) {
                resizeTable(tableSize * 2); // Удвоение размера таблицы
            } else if (loadFactor < 0.3 && tableSize > primeNumber) {
                resizeTable(tableSize / 2); // Уменьшение размера таблицы до половины, но не менее исходного размера
            }
        }


        private void resizeTable(int newSize) {
            List<Message>[] oldTable = hashTable;
            hashTable = new ArrayList[newSize];
            for (int i = 0; i < newSize; i++) {
                hashTable[i] = new ArrayList<>();
            }

            // Перехэшируем элементы из старой таблицы в новую
            for (List<Message> list : oldTable) {
                for (Message message : list) {
                    long hash = hashFunction(message.text, newSize); // Используем новый размер таблицы для вычисления индекса
                    int index = (int) (hash % newSize);
                    hashTable[index].add(message);
                }
            }
            tableSize = newSize; // Обновляем размер таблицы
            primeNumber = newSize;
        }



        static List<Member> initMembers(String input) {
            List<Member> members = new ArrayList<>();
            int[] primes = {11, 11, 11, 11, 11};

            if (input.startsWith("#i")) {
                String[] primeStrings = input.split(" ");
                for (int i = 0; i < 5 && i < primeStrings.length - 1; i++) {
                    primes[i] = Integer.parseInt(primeStrings[i + 1]);
                }
            }
            String[] names = {"Mirek", "Jarka", "Jindra", "Rychlonozka", "Cervenacek"};
            for (int i = 0; i < 5; i++) {
                members.add(new Member(names[i], primes[i]))
                ;
            }
            return members;
        }

        static List<Member> initMembers() {
            List<Member> members = new ArrayList<>();
            int[] primes = {11, 11, 11, 11, 11};


            String[] names = {"Mirek", "Jarka", "Jindra", "Rychlonozka", "Cervenacek"};
            for (int i = 0; i < 5; i++) {
                members.add(new Member(names[i], primes[i]))
                ;
            }
            return members;
        }

        public static Member switchToMember(List<Member> members, int memberIndex) {
            if (memberIndex >= 1 && memberIndex <= members.size()) {
                // Индексация начинается с 0, поэтому вычитаем 1
                System.out.println("Switched to member: " + members.get(memberIndex - 1).name);
                return members.get(memberIndex - 1);
                // Делаем что-то с текущим членом
            } else {
                System.err.println("Error: Chybny vstup!");
            }
            return new Member("ab", 1);
        }

        public String basicInfo() {
            StringBuilder info = new StringBuilder();
            info.append(name).append("\n\t").append(primeNumber).append(" ").append(countUniqueMessages());
            return info.toString();
        }

        public int countUniqueMessages() {
            int count = 0;
            for (List<Message> messages : hashTable) {
                for (Message message : messages) {
                    if (message.repetitions == 1) {
                        count++;
                    }
                }
            }
            return count;
        }



        public static long hashFunction(String text, int primeNumber) {
            int p = 32;
            long hash = 0;
            long power = 1;

            for (int i = 0; i < text.length(); i++) {
                char ch = text.charAt(i);
                int charCode = (ch == ' ') ? 31 : (ch - 'a' + 1); // Кодирование символа в соответствии с заданием
                hash = (hash + charCode * power) % primeNumber;
                power = (power * p) % primeNumber; // Вычисление степени p modulo primeNumber
            }

            return hash;
        }





        public static int power(int base, int exponent) {
            int result = 1;
            while (exponent > 0) {
                if (exponent % 2 == 1) {
                    result *= base;
                }
                base *= base;
                exponent /= 2;
            }
            return result;
        }


    }
}


